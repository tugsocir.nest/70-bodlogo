// Хоорондоо зайтай тоо үг хоёрын тоог нэгээр ихэсгэж өгөгдсөн хэлбэрээр хэвлэ.
#include <iostream>
#include <cmath>
#include <string>
#include <algorithm>
using namespace std;
int main()
{
     string s;
     getline(cin, s);
     int c = 0;
     int l = s.size();
     string num = "", word = "";
     for (int i = 0; i < l; i++)
     {
          if (s[i] >= 48 && s[i] <= 57)
          {
               num += s[i];
          }
          else if (s[i] != ' ')
          {
               word += s[i];
          }
     }
     int x = stoi(num);
     cout << x + 1 << " " << word << endl;
     return 0;
}
