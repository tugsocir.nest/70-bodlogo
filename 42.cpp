// Өгөгдсөн 2 энгийн бутархайн нийлбэрийг ол.
#include <iostream>
#include <cmath>
#include <string>
using namespace std;

int main()
{
     string a, b;
     cin >> a >> b;
     double x1 = 0, y1 = 0;
     string s1 = "", s2 = "";
     for (int i = 0; i < a.size(); i++)
     {
          if (a[i] != '/')
          {
               x1 = x1 + a[i] - 48;
               x1 = x1 * 10;
               if (a[i + 1] == '/')
               {
                    x1 = x1 / 10;
               }
          }
          else
          {
               break;
          }
     }
     for (int i = a.size() - 1; i < a.size(); i--)
     {
          if (a[i] != '/')
          {
               s1 += a[i];
          }
          else
          {
               break;
          }
     }
     for (int i = 0; i < b.size(); i++)
     {
          if (b[i] != '/')
          {
               y1 = y1 + b[i] - 48;
               y1 = y1 * 10;
               if (b[i + 1] == '/')
               {
                    y1 = y1 / 10;
               }
          }
          else
          {
               break;
          }
     }
     for (int i = b.size() - 1; i < b.size(); i--)
     {
          if (b[i] != '/')
          {
               s2 += (b[i]);
          }
          else
          {
               break;
          }
     }
     reverse(s1.begin(), s1.end());
     reverse(s2.begin(), s2.end());
     double x2 = stoi(s1), y2 = stoi(s2);
     // cout << x1 << " " << x2 << endl;
     // cout << y1 << " " << y2 << endl;
     cout << ((x1 * y2) + (x2 * y1)) / (x2 * y2) << endl;
}
