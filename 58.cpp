// Хамгийн урт тогтмол: Өгөгдсөн 1 ба 0 ээр бүтсэн String ээс хамгийн урт нэг тоогоор үргэлжилсэн хэсгийг ол.
#include <iostream>
#include <cmath>
#include <string>
#include <algorithm>
using namespace std;

int main()
{

     string s;
     cin >> s;
     int c = 0, max = 0, index;
     for (int i = 0; i < s.size() - 1; i++)
     {
          if (s[i] == s[i + 1])
          {
               c++;
          }
          else
          {
               if (max < c)
               {
                    max = c;
                    c = 0;
                    index = i;
               }
          }
     }
     cout << index - max + 1 << " " << index + 1 << endl;
     return 0;
}
