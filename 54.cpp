// Өгөгдсөн хаалтууд нь логикийн хувьд зөв эсэхийг тогтоо.
#include <iostream>
#include <cmath>
#include <string>
#include <algorithm>
using namespace std;

int main()
{

     string s;
     cin >> s;
     if (s[0] != '(')
     {
          cout << "NO" << endl;
          return 0;
     }
     int x = 0, x1 = 0;
     for (int i = 0; i < s.size(); i++)
     {
          if (s[i] == '(')
          {
               x++;
          }
          else
          {
               x1++;
          }
     }
     // cout << x << " " << x1 << endl;
     if (x == x1)
     {
          cout << "YES" << endl;
     }
     else
     {
          cout << "NO" << endl;
     }

     return 0;
}
