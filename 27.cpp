// N ширхэг тоо өгөгдсөн бол K тоотой тэнцүү хамгийн бага дугаарыг нь олно.Хэрвээ N ширхэг тоон дунд байхгүй бол `NO` гэж хэвлэнэ.
#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;
int main(){
    int n;
    cin >> n;
    int m[n];
    for (int i = 0; i < n; i++)
    {
        cin >> m[i];
    }
    int k;
    cin >> k;
    for (int i = 0; i < n; i++)
    {
        if (k==m[i])
        {
            cout << i+1 << endl;
            return 0;
        }
    }
    cout << "NO" << endl;
    
    return 0; 
}