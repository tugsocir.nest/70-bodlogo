// N ширхэг тоо агуулах массивын хамгийн их тоог хэвлэ. Массивт өөрчлөлт оруулахгүй.
#include <iostream>
#include <cmath>
#include <string>
using namespace std;
int main()
{
     int n;
     cin >> n;
     int m[n];
     for (int i = 0; i < n; i++)
     {
          cin >> m[i];
     }
     int max = m[0];
     for (int i = 1; i < n; i++)
     {
          if (m[i] > max)
          {
               max = m[i];
          }
     }
     cout << max << endl;
     return 0;
}
