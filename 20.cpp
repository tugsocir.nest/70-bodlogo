// Санамсаргүй өгөгдсөн 10 тооны нийлбэр болон дунджыг ол.
#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;
int main(){
    int m[10];
    double s;
    s = 0;
    for (int i = 0; i < 10; i++)
    {
        int a;
        cin >> a;
        s = s + a;
    }
    cout << s << endl;
    cout << setprecision(2);
    cout << fixed;
    cout << s / 10 << endl;
    return 0; 
}