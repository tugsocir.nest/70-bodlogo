//  Өгөгдсөн тооны цифрүүдийн нийлбэрийг ол.
#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;

int main(){
    long long int a;
    cin >> a;
    int s = 0;
    while (a > 0)
    {
        int x;
        x = a % 10;
        s = s + x;
        a = a / 10;
    }
    cout<<s<<endl;

    return 0; 
}