// N ширхэг тоо өгөгдөхөд нэг элементийн утгыг солиход өсөх эрэмбэтэй болж чадах уу?
// Тийм бол `Yes`, үгүй бол `No` гэж хэвлэнэ.
#include <iostream>
#include <cmath>
#include <string>
using namespace std;
int main()
{

     int n, c = 0;
     cin >> n;
     int m[n];
     cin >> m[0];
     for (int i = 1; i < n; i++)
     {
          cin >> m[i];
          if (m[i] < m[i - 1])
          {
               c++;
          }
     }
     if (c <= 1)
     {
          cout << "YES" << endl;
     }
     else
     {
          cout << "NO" << endl;
     }

     return 0;
}
