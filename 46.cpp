// N ширхэг тоо агуулах массивын тэгш дугаартай тоог 2-т хувааж,
// сондгой дугаартай тоог 7-д хувааж ногдворыг сольж тавь.
// Хэрвээ N нь сондгой бол сүүлийн тоог 2-т хувааж ногдворыг нь тавина.
#include <iostream>
#include <cmath>
#include <string>
using namespace std;
int main()
{
     int n;
     cin >> n;
     int m[n];
     for (int i = 0; i < n; i++)
     {
          cin >> m[i];
     }
     for (int i = 0; i < n; i++)
     {
          if (i % 2 == 0)
          {
               m[i] = m[i] % 2;
          }
          else
          {
               m[i] = m[i] % 7;
          }
     }
     if (n % 2 == 1)
     {
          m[n - 1] %= 2;
     }
     for (int i = 0; i < n; i++)
     {
          cout << m[i] << " ";
     }
     cout << endl;
     return 0;
}
