// Тоо 2-тын зэрэг мөн эсэхийг тогтоо. (функц ашиглахгүй)
#include <iostream>
using namespace std;

int main()
{
     int n;
     cin >> n;
     if (n == 1)
     {
          cout << "YES" << endl;
          return 0;
     }
     else if (n == 0)
     {
          cout << "NO" << endl;
          return 0;
     }
     while (true)
     {
          if (n % 2 == 0)
          {
               n = n / 2;
               if (n == 1)
               {
                    cout << "YES" << endl;
                    break;
               }
          }
          else
          {
               cout << "NO" << endl;
               break;
          }
     }

     return 0;
}