// холбосон гүүртэй 2 байшин байжээ.
// Гүүрээр гарахад 2Ж энерги 1 давхар өгсөхөд 3Ж энерги 1 давхар буухад 1Ж энерги шаардагддаг бол
// Эхний байшингын а давхраас 2 дахь байшингын b давхарт очиход хэдэн Ж энерги шаардлагатай бэ.
#include <iostream>
#include <cmath>
#include <string>
#include <algorithm>
#include <iomanip>
using namespace std;
int main()
{
     int h, a, b;
     int s = 0;
     cin >> h >> a >> b;
     if (h > a)
     {
          s = s + (h - a) * 3;
     }
     else
     {
          s = s + (a - h) * 1;
     }
     if (h > b)
     {
          s = s + (h - b) * 1;
     }
     else
     {
          s = s + (b - h) * 3;
     }
     cout << s + 2 << endl;
     return 0;
}
