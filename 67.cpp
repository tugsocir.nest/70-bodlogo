// Өгөгдсөн string палиндром эсэхийг хэвлэ.
#include <iostream>
#include <cmath>
#include <string>
#include <algorithm>
using namespace std;
int main()
{
     string s;
     cin >> s;
     string s1 = s;
     reverse(s.begin(), s.end());
     if (s == s1)
     {
          cout << "YES" << endl;
     }
     else
     {
          cout << "NO" << endl;
     }

     return 0;
}
