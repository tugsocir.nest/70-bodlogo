// Өгөгдсөн string д 20145 гэсэн тоо тааралдвал Yes, үгүй бол No гэж хэвлэ.
#include <iostream>
#include <cmath>
#include <string>
#include <algorithm>
using namespace std;
int main()
{
     string s;
     cin >> s;
     for (int i = 0; i < s.size() - 4; i++)
     {
          if (s[i] == '2' && s[i + 1] == '0' && s[i + 2] == '1' && s[i + 3] == '4' && s[i + 4] == '5')
          {
               cout << "YES" << endl;
               return 0;
          }
     }
     cout << "NO" << endl;
     return 0;
}
