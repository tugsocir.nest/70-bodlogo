//  Өгөгдсөн хоёр тооны хамгийн их ерөнхий хуваагчийг ол.
#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;

int main(){
    int a, b;
    cin >> a >> b;
    int min;
    if (a < b)
    {
       min = a;
    }
    else{
        min = b;
    }
    while (true)
    {
        if (min % a == 0 && min % b == 0)
        {
            cout << min << endl;
            break;
        }
        else{
            min--;
        }
    }
    
    
    return 0; 
}