
// Өгөгдсөн N тооноос хэдэн ижил хос тоо байгааг ол.
#include <iostream>
#include <cmath>
#include <string>
#include <algorithm>
#include <iomanip>
using namespace std;
int main()
{
     int n;
     cin >> n;
     int m[n], c = 0;
     for (int i = 0; i < n; i++)
     {
          cin >> m[i];
     }
     for (int i = 0; i < n; i++)
     {
          for (int j = i + 1; j < n; j++)
          {
               if (m[i] == m[j])
               {
                    c++;
               }
          }
     }

     cout << c << endl;
     return 0;
}
