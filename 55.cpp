// Код нь хүний нэрний угтвар ба овогийн угтварийн залгавараас тогтдог.
// 2 угтвар хоосон байж болохгүй.
// Хүн бүрт олон янзын код байж болох ба нэр овгийг өгөхөд,
// цагаан толгойн дараллаараа байж болох хамгийн эрт таарах кодыг буцаа.
#include <iostream>
#include <cmath>
#include <string>
#include <algorithm>
using namespace std;

int main()
{

     char x, y;
     cin >> x >> y;
     cout << min(x, y) << endl;

     return 0;
}
