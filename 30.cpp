//N, K хоёр тоо өгөгдөв.1-ээс N хүртэлх тоонуудаас цифрүүдийн нийлбэр нь K-д хуваагдах тоонуудын тоог ол.
#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;
int Tsifr(int n)
{
    int s = 0;
    while (n > 0)
    {
        s = s + n % 10;
        n = n / 10;
    }
    return s;
}
int main()
{
    int n, k;
    int c = 0;
    cin >> n >> k;
    for (int i = 1; i <= n; i++)
    {
        if (Tsifr(i) % k == 0)
        {
            c++;
        }
    }
    cout << c << endl;
    return 0;
}