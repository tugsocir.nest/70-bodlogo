// Өгөгдсөн string д орсон нэгээс илүү үргэлжилсэн хоосон зайнуудыг ганц хоосон зай болгож хэвлэ.
#include <iostream>
#include <cmath>
#include <string>
#include <algorithm>
using namespace std;
int main()
{
     string s;
     getline(cin, s);
     int c = 0;
     int l = s.size();
     for (int i = 0; i < l; i++)
     {
          if (s[i] == ' ' && s[i + 1] == ' ')
          {
               for (int j = i; j < l - 1; j++)
               {
                    s[j] = s[j + 1];
               }
               i--;
               l--;
               s.resize(l);
          }
     }
     cout << s << endl;
     return 0;
}
