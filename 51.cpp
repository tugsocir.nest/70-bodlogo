// N ширхэг тоо өгөгдөхөд бүх тэгш дугаартай сондгой тоонуудын дугаарын нийлбэрийг хэвлэ.
#include <iostream>
#include <cmath>
#include <string>
#include <algorithm>
using namespace std;
int main()
{
     int n, s = 0;
     cin >> n;
     int m[n];
     for (int i = 1; i <= n; i++)
     {
          cin >> m[i];
     }
     for (int i = 1; i <= n; i++)
     {
          if (i % 2 == 0 && m[i] % 2 == 1)
          {
               s += i;
          }
     }
     cout << s << endl;
     return 0;
}
