// Өгөгдсөн А тоо анхны тоо мөн үү.
#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;
bool first(int a)
{
    int i = 1, s = 0;
    while (a >= i)
    {
        if (a % i == 0)
        {
            s = s + 1;
        }
        i++;
    }
    if (s == 2)
    {
        return true;
    }
    else
    {
        return false;
    }
}
int main(){
    int a;
    cin >> a;
    if (first(a))
    {
        cout << "YES" << endl;
    }
    else{
        cout << "NO" << endl;
    }
    
    return 0; 
}