//Өгөгдсөн N тооноос зэргэлдээ хэдэн ижил хос тоо байгааг ол.
#include <iostream>
#include <cmath>
#include <string>
#include <algorithm>
#include <iomanip>
using namespace std;
int main()
{
     int n;
     cin >> n;
     int m[n];
     for (int i = 0; i < n; i++)
     {
          cin >> m[i];
     }
     int c = 0;
     for (int i = 0; i < n - 1; i++)
     {
          if (m[i] == m[i + 1])
          {
               c++;
          }
     }
     cout << c << endl;
     return 0;
}
