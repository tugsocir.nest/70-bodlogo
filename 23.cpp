// Өгөгдсөн 2 тооны хамгийн бага ерөнхий хуваагдагчийг ол.
#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;

int main(){
    int a, b;
    cin >> a >> b;
    int max;
    if (a > b)
    {
       max = a;
    }
    else{
        max = b;
    }
    while (true)
    {
        if (max % a == 0 && max % b == 0)
        {
            cout << max << endl;
            break;
        }
        else{
            max++;
        }
    }
    return 0; 
}