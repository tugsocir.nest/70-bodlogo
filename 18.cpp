//Өгөгдсөн А он нь өндөр жил мөн үү. Өндөр жил гэдэг нь 4-т хуваагддаг, 100 хуваагддаггүй, 400 жилд хуваагддаг жилийг хэлдэг.
#include <iostream>
#include <cmath>
using namespace std;
int main(){
    int a;
    cin >> a;
    if (a%4==0 && a % 100 != 0){
        cout << "YES" << endl;
    }
    else {
        cout << "NO" << endl;
    }
    
    return 0; 
}