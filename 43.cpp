// N, M, K гурван тоо өгөгдөв.
// N-ээс M хүртэлх тоонуудаас цифрүүдийн нийлбэр нь K-д хуваагдах тоонуудын тоог ол.
#include <iostream>
#include <cmath>
#include <string>
using namespace std;
int Tsifr(int n)
{
     int s = 0;
     while (n > 0)
     {
          s = s + n % 10;
          n = n / 10;
     }
     return s;
}
int main()
{
     int n, m, k;
     cin >> n >> m >> k;
     int c = 0;
     if (n == 0)
     {
          n = 1;
     }
     for (int i = n; i <= m; i++)
     {

          if (Tsifr(i) % k == 0)
          {

               c++;
          }
     }
     cout << c << endl;
}
