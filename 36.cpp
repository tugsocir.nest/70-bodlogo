// n x m шатрын хөлөг дээр бие биенээ идэхгүй хэдэн хүү тавьж болох вэ?
// (Хүү зөвхөн баруун дээд эсвэл зүүн дээд нүдэн дэх хүүгээ идэж чадна)
#include <iostream>
using namespace std;
int main()
{
     int n, m, z;
     cin >> n >> m;
     if (min(n, m) % 2 == 0)
     {
          z = min(n, m) / 2;
     }
     else
     {
          z = min(n, m) / 2 + 1;
     }

     cout << max(n, m) * z << endl;
}
