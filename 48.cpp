// Өгөгдсөн N ширхэг тоо өсөх эрэмбэтэй юу? Тийм бол `Yes`, үгүй бол `No` гэж хэвлэнэ.
#include <iostream>
#include <cmath>
#include <string>
using namespace std;
int main()
{

     int n;
     cin >> n;
     int m[n];
     cin >> m[0];
     for (int i = 1; i < n; i++)
     {
          cin >> m[i];
          if (m[i] < m[i - 1])
          {
               cout << "NO" << endl;
               return 0;
          }
     }
     cout << "YES" << endl;

     return 0;
}
