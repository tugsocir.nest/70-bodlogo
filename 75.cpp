// Өгөгдсөн N тоог дараахь эрэмбээр хэвлэ.
// Эхлээд сондгой дугаарт байгаа тоонууд, дараагаар нь тэгш дугаарт байгаа тоонууд.
#include <iostream>
#include <cmath>
#include <string>
#include <algorithm>
#include <iomanip>
using namespace std;
int main()
{
     int n;
     cin >> n;
     int m[n];
     for (int i = 0; i < n; i++)
     {
          cin >> m[i];
     }
     int m1[n];
     int j = 0;
     for (int i = 0; i < n; i += 2)
     {
          m1[j] = m[i];
          j++;
     }
     for (int i = 1; i < n; i += 2)
     {
          m1[j] = m[i];
          j++;
     }
     for (int i = 0; i < n; i++)
     {
          cout << m1[i] << " ";
     }
     cout << endl;
     return 0;
}
