// 2^0+2^1+ 2^2+ 2^3 … 2^n нийлбэрийг хамгийн хурданаараа ол.
#include <iostream>
#include <cmath>
using namespace std;
int main()
{
     int n;
     cin >> n;
     cout << (1 * (pow(2, n + 1) - 1)) / (2 - 1) << endl;
     return 0;
}
