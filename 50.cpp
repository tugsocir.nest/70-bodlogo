// N ширхэг тоо өгөгдөхөд хамгийн их зөрүүтэй 2 элементийг олж дугаартай нь хэвлэнэ.
#include <iostream>
#include <cmath>
#include <string>
#include <algorithm>
using namespace std;
int main()
{
     int n;
     cin >> n;
     int m[n];
     for (int i = 0; i < n; i++)
     {
          cin >> m[i];
     }
     int min = m[0], max = m[0], i1, i2;
     for (int i = 0; i < n; i++)
     {
          if (m[i] < min)
          {
               min = m[i];
               i1 = i;
          }
          if (m[i] > max)
          {
               max = m[i];
               i2 = i;
          }
     }
     cout << max << " " << min << endl;
     cout << i2 + 1 << " " << i1 + 1 << endl;
     return 0;
}
