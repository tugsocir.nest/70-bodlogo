// Тоон дараалал өгөгдөв. Дараалал дахь хамгийн их тоо дараалалд хэд байгааг тоол.
#include <iostream>
#include <cmath>
#include <string>
#include <algorithm>
#include <iomanip>
using namespace std;
int main()
{
     int n, c = 1;
     cin >> n;
     int m[n];
     for (int i = 0; i < n; i++)
     {
          cin >> m[i];
     }
     sort(m, m + n, greater<int>());
     for (int i = 0; i < n; i++)
     {
          cout << m[i] << " ";
     }
     cout << endl;
     for (int i = 0; i < n - 1; i++)
     {
          if (m[i] == m[i + 1])
          {
               c++;
          }
          else
          {
               break;
          }
     }
     cout << c << endl;
     return 0;
}
