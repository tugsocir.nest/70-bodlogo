// Өгөгдсөн N тоог дараахь байдлаар гарга.
// Эхлээд бүх сондгой тоог өгөгдсөн дарааллынх нь дагуу,
// дараагаар нь бүх тэгш тоонуудыг өгөгдсөн дарааллаар нь.
#include <iostream>
#include <cmath>
#include <string>
#include <algorithm>
#include <iomanip>
using namespace std;
int main()
{
     int n;
     cin >> n;
     int m[n];
     for (int i = 0; i < n; i++)
     {
          cin >> m[i];
     }
     int m1[n];
     int j = 0;
     for (int i = 0; i < n; i++)
     {
          if (m[i] % 2 == 1)
          {
               m1[j] = m[i];
               j++;
          }
     }
     for (int i = 0; i < n; i++)
     {
          if (m[i] % 2 == 0)
          {
               m1[j] = m[i];
               j++;
          }
     }
     for (int i = 0; i < j; i++)
     {
          cout << m1[i] << " ";
     }
     cout << endl;
     return 0;
}
