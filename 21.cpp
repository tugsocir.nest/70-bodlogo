//1 - 100 хүртэлх 4-д хуваагддаг 16-д хуваагдаггүй хэдэн тоо байна вэ. 476 - 2980 хүртэлх 4-д хуваагддаг 16-д хуваагдаггүй хэдэн тоо байна вэ.
#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;
int main(){
    int x=0;
    for (int i = 1; i <= 100; i++){
        if (i % 4 == 0 && i % 16 == 0){
            x++;
        }
    }
    cout << x << endl;
    int y = 0;
    for (int i = 476; i <=2980; i++)
    {
       if (i % 4 == 0 && i % 16 == 0){
            y++;
        }
    }
    cout << y << endl;
    return 0; 
}