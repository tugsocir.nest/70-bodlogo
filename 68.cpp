// Гурвалжны гурван тал өгөгдсөн бол талбайг 3 орний нарийвчлалтайгаар ол.
#include <iostream>
#include <cmath>
#include <string>
#include <algorithm>
#include <iomanip>
using namespace std;
int main()
{
     double a, b, c;
     cin >> a >> b >> c;
     double p = (a + b + c) / 2;
     cout << fixed;
     cout << setprecision(3);
     // cout << p << endl;
     // cout << p - a << " " << p - b << " " << p - c << endl;
     cout << sqrt(p * (p - a) * (p - b) * (p - c)) << endl;

     return 0;
}
